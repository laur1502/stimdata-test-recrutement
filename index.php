<!DOCTYPE html>
<html>
<head>
	<title>Connexion - Stimdata Test</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap" rel="stylesheet">
</head>
<body>
	<section id="connexion_container">
		<h1>Test de connexion - STIMDATA</h1>
			<form id="form_connexion" action="action.php" method="post">
				<label for="login_user">Login : </label>
				<input id="login_user" type="text" name="login_user" required/>
				<label for="password_user">Mot de passe : </label>
				<input id="password_user" type="password" name="password_user" required/>

				<button name="submit">Se connecter</button>	 
			</form>
	</section>
</body>
</html>



